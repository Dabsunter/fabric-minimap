package fr.dabsunter.blc.minimap;

import io.github.cottonmc.cotton.gui.widget.WWidget;

import java.util.HashSet;

public class InGameHudTicker {

    private static final HashSet<WWidget> widgets = new HashSet<>();

    public static void register(WWidget widget) {
        widgets.add(widget);
    }

    public static void unregister(WWidget widget) {
        widgets.remove(widget);
    }

    public static void tick() {
        for (WWidget widget : widgets)
            widget.tick();
    }
}
