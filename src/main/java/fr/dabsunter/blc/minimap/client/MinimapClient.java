package fr.dabsunter.blc.minimap.client;

import fr.dabsunter.blc.minimap.InGameHudTicker;
import fr.dabsunter.blc.minimap.gui.MinimapPanel;
import io.github.cottonmc.cotton.gui.client.CottonHud;
import net.fabricmc.api.ClientModInitializer;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.fabricmc.fabric.api.client.keybinding.v1.KeyBindingHelper;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.options.KeyBinding;
import net.minecraft.util.Identifier;
import org.lwjgl.glfw.GLFW;

@Environment(EnvType.CLIENT)
public class MinimapClient implements ClientModInitializer {

    public static final KeyBinding ZOOM_IN = new KeyBinding("key.minimap.zoom.in", GLFW.GLFW_KEY_KP_ADD, "key.minimap");
    public static final KeyBinding ZOOM_OUT = new KeyBinding("key.minimap.zoom.out", GLFW.GLFW_KEY_KP_SUBTRACT, "key.minimap");

    public static final Identifier ROUND_MASK = new Identifier("minimap", "textures/gui/round_mask.png");

    @Override
    public void onInitializeClient() {
        MinecraftClient mc = MinecraftClient.getInstance();

        MinimapPanel widget = new MinimapPanel();
        CottonHud.INSTANCE.add(widget, -widget.getWidth() - 10,10);
        InGameHudTicker.register(widget);

        KeyBindingHelper.registerKeyBinding(ZOOM_IN);
        KeyBindingHelper.registerKeyBinding(ZOOM_OUT);


    }
}
