package fr.dabsunter.blc.minimap.mavicpro;

import com.mojang.blaze3d.platform.GlStateManager;
import com.mojang.blaze3d.systems.RenderSystem;
import fr.dabsunter.blc.minimap.render.MavicProCamera;
import io.github.cottonmc.cotton.gui.client.ScreenDrawing;
import io.github.cottonmc.cotton.gui.widget.WWidget;
import io.github.cottonmc.cotton.gui.widget.data.HorizontalAlignment;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.gl.Framebuffer;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.render.*;
import net.minecraft.client.texture.NativeImage;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.client.util.math.Vector3f;
import net.minecraft.util.Util;
import net.minecraft.util.math.Matrix4f;
import org.lwjgl.opengl.GL11;

import java.io.File;
import java.io.IOException;

import static net.minecraft.client.MinecraftClient.IS_SYSTEM_MAC;

public class MavicProMapWidget extends WWidget {

    private static Camera mavicPro = new MavicProCamera();
    public static Framebuffer FB;
    public static boolean INJECT = false;
    private static double flyHeight = 15.;
    private boolean needRender = true;

    public MavicProMapWidget() {
        setSize(128,128);
    }

    @Override
    public boolean canResize() {
        return false;
    }

    @Override
    public void paint(MatrixStack matrices, int x, int y, int mouseX, int mouseY) {

        MinecraftClient mc = MinecraftClient.getInstance();

//        int skybuf = GlStateManager.genFramebuffers();
//        GlStateManager.bindFramebuffer(GL_FRAMEBUFFER, skybuf);
//
//        int renderedTexture = GlStateManager.genTextures();
//        GlStateManager.bindTexture(renderedTexture);
//
//        GlStateManager.texImage2D(GL_TEXTURE_2D, 0, GL_RGB, 128, 128, 0, GL_RGB,
//                GL_UNSIGNED_BYTE, null);
//
//        GlStateManager.texParameter(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
//        GlStateManager.texParameter(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
//
//        GL33.glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, renderedTexture, 0);
//
//        GL33.glDrawBuffers(GL_COLOR_ATTACHMENT0);
//
//        GlStateManager.bindFramebuffer(GL_FRAMEBUFFER, skybuf);
//        GlStateManager.viewport(0,0,128,128);

//        if (mc.currentScreen != lastScreen) {
//            System.out.println("New screen: " + lastScreen + " -> " + mc.currentScreen);
//            lastScreen = mc.currentScreen;
//        }

//        if (needRender) {
//            tick();
//            needRender = false;
//        }
        //tick();

        ScreenDrawing.drawStringWithShadow(matrices, "test", HorizontalAlignment.CENTER, 50 , 50, width, 0xFFFFFF);
        if (true || mc.currentScreen == null) {

            FB.beginRead();


            if (width <= 0) width = 1;
            if (height <= 0) height = 1;

            int color = 0xFFFFFF;
            float opacity = 1.0F;

            float r = (color >> 16 & 255) / 255.0F;
            float g = (color >> 8 & 255) / 255.0F;
            float b = (color & 255) / 255.0F;
            Tessellator tessellator = Tessellator.getInstance();
            BufferBuilder buffer = tessellator.getBuffer();
            RenderSystem.enableBlend();
            //GlStateManager.disableTexture2D();
            RenderSystem.blendFuncSeparate(GlStateManager.SrcFactor.SRC_ALPHA, GlStateManager.DstFactor.ONE_MINUS_SRC_ALPHA, GlStateManager.SrcFactor.ONE, GlStateManager.DstFactor.ZERO);
            buffer.begin(GL11.GL_QUADS, VertexFormats.POSITION_COLOR_TEXTURE); //I thought GL_QUADS was deprecated but okay, sure.
            buffer.vertex(x,         y + height, 0).color(r, g, b, opacity).texture(0, 128).next();
            buffer.vertex(x + width, y + height, 0).color(r, g, b, opacity).texture(128, 128).next();
            buffer.vertex(x + width, y,          0).color(r, g, b, opacity).texture(128, 0).next();
            buffer.vertex(x,         y,          0).color(r, g, b, opacity).texture(0, 0).next();
            tessellator.draw();
            //GlStateManager.enableTexture2D();
            RenderSystem.disableBlend();

            FB.endRead();

            //ScreenDrawing.texturedRect(x, y, getWidth()/2, 20, AbstractButtonWidget.WIDGETS_LOCATION, buttonLeft, buttonTop, buttonLeft+buttonWidth, buttonTop+buttonHeight, 0xFFFFFFFF);
        }
    }

    @Override
    public void tick() {
        ticc(0);
    }

    public static void ticc(long time) {
        MinecraftClient mc = MinecraftClient.getInstance();

        mavicPro = mc.gameRenderer.getCamera();
        //mavicPro.update(mc.gameRenderer.getCamera(), flyHeight);

        INJECT = true;

        if (FB == null)
            FB = new Framebuffer(128, 128, true, false);


        RenderSystem.clear(16640, IS_SYSTEM_MAC);

        FB.beginWrite(true);

        BackgroundRenderer.method_23792();
        RenderSystem.enableTexture();
        RenderSystem.enableCull();


        //RenderSystem.viewport(0, 0, 128, 128);

        MatrixStack submatrices = new MatrixStack();

        submatrices.peek().getModel().multiply(
                Matrix4f.viewboxMatrix(90., 1.0F, 0.05F, mc.gameRenderer.getViewDistance() * 4.0F));

        Matrix4f matrix4f = submatrices.peek().getModel();

        mc.gameRenderer.loadProjectionMatrix(matrix4f);

        submatrices.multiply(Vector3f.POSITIVE_X.getDegreesQuaternion(mavicPro.getPitch()));
        submatrices.multiply(Vector3f.POSITIVE_Y.getDegreesQuaternion(mavicPro.getYaw() + 180.0F));

        mc.worldRenderer.render(submatrices, mc.getTickDelta(), Util.getMeasuringTimeNano(), false, mavicPro,
                mc.gameRenderer, mc.gameRenderer.getLightmapTextureManager(), matrix4f);

        //GlStateManager.bindFramebuffer(GL_FRAMEBUFFER, 0);
        FB.endWrite();

        INJECT = false;
//
//        FB.beginRead();
//        NativeImage img = new NativeImage(128, 128, false);
//        img.loadFromTextureImage(0, false);
//
//        try {
//            img.writeFile(new File("test.png"));
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//        FB.endRead();

        mc.getFramebuffer().beginWrite(false);
    }
}
