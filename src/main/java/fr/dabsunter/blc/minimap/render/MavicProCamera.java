package fr.dabsunter.blc.minimap.render;

import net.minecraft.client.render.Camera;
import org.lwjgl.opengl.GL11;

public class MavicProCamera extends Camera {

    public void update(Camera camera, double height) {

        setPos(camera.getPos().add(0., height, 0.));
        setRotation(camera.getYaw(), 0.f);
    }
}
