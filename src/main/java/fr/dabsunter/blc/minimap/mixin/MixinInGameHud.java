package fr.dabsunter.blc.minimap.mixin;

import fr.dabsunter.blc.minimap.InGameHudTicker;
import net.minecraft.client.gui.hud.InGameHud;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(InGameHud.class)
public class MixinInGameHud {

    @Inject(method = "tick", at = @At("RETURN"))
    protected void onTick(CallbackInfo ci) {
        InGameHudTicker.tick();
    }

}
