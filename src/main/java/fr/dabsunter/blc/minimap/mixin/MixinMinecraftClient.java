package fr.dabsunter.blc.minimap.mixin;

import fr.dabsunter.blc.minimap.mavicpro.MavicProMapWidget;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.gl.Framebuffer;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin(MinecraftClient.class)
public class MixinMinecraftClient {

    @Inject(method = "getFramebuffer", at = @At("HEAD"), cancellable = true)
    protected void onGetFramebuffer(CallbackInfoReturnable<Framebuffer> cir) {
        if (MavicProMapWidget.INJECT)
            cir.setReturnValue(MavicProMapWidget.FB);
    }
}
