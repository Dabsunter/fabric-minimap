package fr.dabsunter.blc.minimap.gui;

import fr.dabsunter.blc.minimap.cartographer.CartographerMapWidget;
import fr.dabsunter.blc.minimap.mavicpro.MavicProMapWidget;
import io.github.cottonmc.cotton.gui.client.BackgroundPainter;
import io.github.cottonmc.cotton.gui.widget.WLabel;
import io.github.cottonmc.cotton.gui.widget.WPlainPanel;
import io.github.cottonmc.cotton.gui.widget.data.HorizontalAlignment;
import io.github.cottonmc.cotton.gui.widget.data.VerticalAlignment;

public class MinimapPanel extends WPlainPanel {

    public MinimapPanel() {
        setSize(128, 128);
        //setBackgroundPainter(BackgroundPainter.VANILLA);
        add(new CartographerMapWidget(), 0, 0);

        add(new WLabel("Hello World", 0xFFFFFF)
                .setHorizontalAlignment(HorizontalAlignment.CENTER)
                .setVerticalAlignment(VerticalAlignment.BOTTOM), width/2, height);
    }
}
