package fr.dabsunter.blc.minimap.cartographer;

import net.minecraft.client.render.VertexConsumerProvider;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.client.util.math.Vector3f;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.world.World;
import net.minecraft.world.chunk.Chunk;

import java.util.HashMap;

public class MapCanvas {

    private final World world;
    private final HashMap<ChunkPos, Tile> tiles = new HashMap<>();

    public MapCanvas(World world) {
        this.world = world;
    }

    public void update(ChunkPos center, int radius) {
        ChunkPos.stream(center, radius)
                .forEach(pos -> {
                    Tile tile = getTile(pos);
                    if (tile != null) tile.update();
                });
    }

    public void draw(MatrixStack matrices, VertexConsumerProvider provider, double x, double z, float yaw, float scale) {
        matrices.push();
        matrices.multiply(Vector3f.NEGATIVE_Z.getDegreesQuaternion(yaw + 180F));
        matrices.scale(scale, scale, 1);

        ChunkPos.stream(new ChunkPos((int)x >> 4, (int)z >> 4), Math.max((int) (8F/scale), 1))
                .forEach(pos -> {
                    Tile tile = getTile(pos);
                    if (tile != null) {
                        matrices.push();
                        matrices.translate((pos.x << 4) - x, (pos.z << 4) - z, 0);
                        tile.draw(matrices, provider);
                        matrices.pop();
                    }
                });

        matrices.pop();
    }

    private Tile getTile(int x, int z) {
        return getTile(new ChunkPos(x, z));
    }

    private Tile getTile(ChunkPos pos) {
        return tiles.computeIfAbsent(pos, p -> {
            Chunk chunk = (Chunk) world.getExistingChunk(p.x, p.z);
            return chunk == null ? null : new Tile(chunk);
        });
    }
}
