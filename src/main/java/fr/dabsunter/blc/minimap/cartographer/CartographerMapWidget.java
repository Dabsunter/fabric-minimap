package fr.dabsunter.blc.minimap.cartographer;

import com.mojang.blaze3d.platform.GlStateManager;
import com.mojang.blaze3d.systems.RenderSystem;
import fr.dabsunter.blc.minimap.client.MinimapClient;
import io.github.cottonmc.cotton.gui.client.ScreenDrawing;
import io.github.cottonmc.cotton.gui.widget.WWidget;
import io.github.cottonmc.cotton.gui.widget.data.HorizontalAlignment;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.render.BufferBuilder;
import net.minecraft.client.render.Tessellator;
import net.minecraft.client.render.VertexConsumerProvider;
import net.minecraft.client.render.VertexFormats;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.client.util.math.Vector3f;
import net.minecraft.util.Identifier;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.Matrix4f;
import net.minecraft.util.registry.RegistryKey;
import net.minecraft.world.World;
import net.minecraft.world.dimension.DimensionType;
import org.lwjgl.opengl.GL11;

public class CartographerMapWidget extends WWidget {

    private static final Identifier MAP_ICONS_TEXTURE = new Identifier("textures/map/map_icons.png");

    private static final int UPDATE_TIMER = 20;
    private static final float SCALE_FACTOR = MathHelper.SQUARE_ROOT_OF_TWO;
    private MapCanvas canvas;
    private World lastWorld;
    private int timer = 0;
    private float scale = SCALE_FACTOR;

    public CartographerMapWidget() {
        setSize(128, 128);
    }

    @Override
    public void paint(MatrixStack matrices, int x, int y, int mouseX, int mouseY) {
        if (canvas == null)
            return;

        MinecraftClient mc = MinecraftClient.getInstance();

        mc.getTextureManager().bindTexture(MinimapClient.ROUND_MASK);
        Tessellator tessellator = Tessellator.getInstance();
        BufferBuilder buffer = tessellator.getBuffer();
        RenderSystem.depthMask(true);
        RenderSystem.enableBlend();
        //RenderSystem.blendFuncSeparate(GlStateManager.SrcFactor.SRC_ALPHA, GlStateManager.DstFactor.ONE_MINUS_SRC_ALPHA, GlStateManager.SrcFactor.ONE, GlStateManager.DstFactor.ZERO);
        RenderSystem.defaultBlendFunc();
        buffer.begin(GL11.GL_QUADS, VertexFormats.POSITION_TEXTURE); //I thought GL_QUADS was deprecated but okay, sure.
        buffer.vertex(x,         y + height, 0).texture(0, 1).next();
        buffer.vertex(x + width, y + height, 0).texture(1, 1).next();
        buffer.vertex(x + width, y,          0).texture(1, 0).next();
        buffer.vertex(x,         y,          0).texture(0, 0).next();
        tessellator.draw();
        //GlStateManager.enableTexture2D();

        RenderSystem.depthMask(false);
        RenderSystem.blendFunc(GlStateManager.SrcFactor.DST_ALPHA, GlStateManager.DstFactor.ONE_MINUS_DST_ALPHA);
        RenderSystem.enableDepthTest();
        RenderSystem.depthFunc(GL11.GL_EQUAL);

        matrices.push();
        matrices.translate(x + 64, y + 64, 0);

        VertexConsumerProvider.Immediate immediate = VertexConsumerProvider.immediate(Tessellator.getInstance().getBuffer());
        canvas.draw(matrices, immediate,
                mc.player.getX(), mc.player.getZ(), mc.player.yaw, scale);
        immediate.draw();

        RenderSystem.disableDepthTest();
        RenderSystem.depthFunc(GL11.GL_LESS);
        RenderSystem.defaultBlendFunc();

        Matrix4f matrix = matrices.peek().getModel();

        MinecraftClient.getInstance().getTextureManager().bindTexture(MAP_ICONS_TEXTURE);

        buffer.begin(GL11.GL_QUADS, VertexFormats.POSITION_TEXTURE); //I thought GL_QUADS was deprecated but okay, sure.
        buffer.vertex(matrix, 8.5F, -8.0F, 0).texture(0, 0).next();
        buffer.vertex(matrix, -7.5F, -8.0F, 0).texture(1F/16F, 0).next();
        buffer.vertex(matrix, -7.5F, 8.0F, 0).texture(1F/16F, 1/16F).next();
        buffer.vertex(matrix, 8.5F, 8.0F, 0).texture(0, 1F/16F).next();
        tessellator.draw();

        matrices.push();
        matrices.multiply(Vector3f.NEGATIVE_Z.getDegreesQuaternion(mc.player.yaw + 180F));
        matrices.translate(0, -4, 0);

        ScreenDrawing.drawString(matrices, "E", HorizontalAlignment.CENTER, 56, 0, 0, 0xFFFFFF);
        ScreenDrawing.drawString(matrices, "S", HorizontalAlignment.CENTER, 0, 56, 0, 0xFFFFFF);
        ScreenDrawing.drawString(matrices, "W", HorizontalAlignment.CENTER, -56, 0, 0, 0xFFFFFF);
        ScreenDrawing.drawString(matrices, "N", HorizontalAlignment.CENTER, 0, -56, 0, 0xFFFFFF);

        matrices.pop();

        RenderSystem.disableBlend();

        matrices.pop();

    }

    @Override
    public void tick() {
        while (MinimapClient.ZOOM_IN.wasPressed())
            scale *= SCALE_FACTOR;

        while (MinimapClient.ZOOM_OUT.wasPressed())
            scale /= SCALE_FACTOR;

        MinecraftClient mc = MinecraftClient.getInstance();

        if (mc.world == null || ++timer % UPDATE_TIMER != 0)
            return;

        if (!mc.world.equals(lastWorld)) {
            canvas = new MapCanvas(mc.world);
            lastWorld = mc.world;
            System.out.println("New world !");
        }

        canvas.update(new ChunkPos(mc.player.chunkX, mc.player.chunkZ), (int) (mc.gameRenderer.getViewDistance()/16));
        timer = 0;
    }
}
