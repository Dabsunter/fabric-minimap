package fr.dabsunter.blc.minimap.cartographer;

import com.mojang.blaze3d.platform.GlStateManager;
import com.mojang.blaze3d.systems.RenderSystem;
import net.minecraft.block.Material;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.render.*;
import net.minecraft.client.texture.NativeImage;
import net.minecraft.client.texture.NativeImageBackedTexture;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.util.Identifier;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.util.math.Matrix4f;
import net.minecraft.world.Heightmap;
import net.minecraft.world.chunk.Chunk;
import org.lwjgl.opengl.GL11;

import java.io.File;
import java.io.IOException;

public class Tile {

    private final Chunk chunk;
    private final NativeImageBackedTexture tile;
    private final RenderLayer renderLayer;
    private final Identifier identifier;

    public Tile(Chunk chunk) {
        this.chunk = chunk;
        tile = new NativeImageBackedTexture(16, 16, true);
        identifier = MinecraftClient.getInstance().getTextureManager()
                .registerDynamicTexture(mkId(chunk), tile);
        renderLayer = RenderLayer.getText(identifier);
    }

    public void update() {
        Heightmap heights = chunk.getHeightmap(Heightmap.Type.WORLD_SURFACE);

        BlockPos.Mutable pos = new BlockPos.Mutable();
        for (int x = 0; x < 16; x++) {
            for (int z = 0; z < 16; z++) {
                Material peak = chunk.getBlockState(pos.set(x, heights.get(x, z) - 1, z)).getMaterial();
                tile.getImage().setPixelColor(x, z, peak.getColor().getRenderColor(1));
                //System.out.println("der kolor: " + peak.getColor().getRenderColor(1));
            }
        }

//        try {
//            tile.getImage().writeFile(new File(mkId(chunk) + ".png"));
//        } catch (IOException e) {
//            e.printStackTrace();
//        }

        tile.upload();
    }

    public void draw(MatrixStack matrices, VertexConsumerProvider provider) {
        Matrix4f matrix = matrices.peek().getModel();
//        VertexConsumer vc = provider.getBuffer(renderLayer);
//        vc.vertex(matrix, 0, 0, 0).color(255, 255, 255, 255).texture(0, 0).light(15728880).next();
//        vc.vertex(matrix, 16, 0, 0).color(255, 255, 255, 255).texture(1, 0).light(15728880).next();
//        vc.vertex(matrix, 16, 16, 0).color(255, 255, 255, 255).texture(1, 1).light(15728880).next();
//        vc.vertex(matrix, 0, 16, 0).color(255, 255, 255, 255).texture(0, 1).light(15728880).next();

        MinecraftClient.getInstance().getTextureManager().bindTexture(identifier);

        Tessellator tessellator = Tessellator.getInstance();
        BufferBuilder buffer = tessellator.getBuffer();
        buffer.begin(GL11.GL_QUADS, VertexFormats.POSITION_TEXTURE); //I thought GL_QUADS was deprecated but okay, sure.
        buffer.vertex(matrix, 0,         16, 0).texture(0, 1).next();
        buffer.vertex(matrix, 16, 16, 0).texture(1, 1).next();
        buffer.vertex(matrix, 16, 0,          0).texture(1, 0).next();
        buffer.vertex(matrix, 0,         0,          0).texture(0, 0).next();
        tessellator.draw();

    }

    private static String mkId(Chunk chunk) {
        ChunkPos pos = chunk.getPos();
        return "dabs_minimap/" + pos.x + "." + pos.z;
    }
}
