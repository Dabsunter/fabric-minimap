![icon](https://gitlab.com/Dabsunter/fabric-minimap/-/raw/master/src/main/resources/assets/minimap/icon.png)

Fabric Minimap
==============
A simple replica of the Minimap Mod

Implementation story
--------------------
I first worked on a more original "MavicPro" implementation, which consisted of a third camera that would render the world from the sky.
This kept me busy from Monday to Saturday without any conclusive result: it was impossible to render in a second framebuffer, the only thing I could get was the color of the sky... Not to mention that once I had bound a foreign frame buffer, even when rebinding the original one, the rendering of the upper layers was no longer possible.
The last time I saw something similar was the RearView Mod in 1.6.4, nothing since :confused:
I can believe that all these classes wrapping native OpenGL functions make life easier for Mojang developers, but without documentation I must admit it becomes a bit mystical :sweat_smile:

Finally, Saturday night, nothing functional, I decided to implement "Cartographer", a much less cool version, but also much less problematic.
It is based on the same logic as the internal maps of the game, drawing in a picture, pixel by pixel a representative color of each block on the surface. I used the "MaterialColor", hence the obvious resemblance with the original maps.
I also add that this is my first Fabric Mod, at the beginning of the week I didn't know what a Mixin was, I only had a vague idea of 4x4 matrix stack usage, and their interest on homogeneous coordinates. Anyway it has been an inspiring week.

Known issues
------------
- If an item held show its name over the hotbar, or you are in F3, then the map is not rendered.
  But if you are in advanced F3 (FPS + TPS graphs), it work properly, even with the item name shown...
  *Probably something with glBlend*
- As there are no restriction on Zoom In/Out, going too far, lead to FPS drops as it will render a **lots** of tiles.
  *Could be easily fixed by clamping the zoom at reasonable values*
- Biomes are not considered, so that it will always show generic MaterialColors.
  *Quite weird in savannas, swamps...*
- Not as cool as I expected :sob: